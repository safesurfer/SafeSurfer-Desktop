<a href="http://www.gnu.org/licenses/gpl-3.0.html">
    <img src="https://img.shields.io/badge/License-GPL%20v3-blue.svg" alt="License" />
</a>
<a href="https://hosted.weblate.org/projects/safe-surfer/translations">
    <img src="https://hosted.weblate.org/widgets/safe-surfer/-/translations/svg-badge.svg" alt="Translation status" />
</a>
<a href="https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases">
    <img src="https://img.shields.io/badge/version-1.0.2-brightgreen.svg" />
</a>
<a href="https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases">
    <img src="https://img.shields.io/badge/build-14-orange.svg" />
</a>

![Safe Surfer logo](./assets/media/icons/png/128x128.png)  
# SafeSurfer-Desktop
Official Safe Surfer desktop app, built with Electron.

<a href="https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases">
    <img src="screenshots/SafeSurfer-Desktop-Activated-Standard.png" />
</a>
<br>
For more screenshots of the app, please refer to the [screenshots](screenshots) folder.  

## Who is [Safe Surfer](http://safesurfer.co.nz)?
Safe Surfer's mission is to keep children, individuals, and organisations safe online. We achieve this by filtering out harmful material that may be found when browsing the internet and switching on safe search for a number of search engines.  
You can read more [here](http://www.safesurfer.co.nz/the-cause).  

## App information
Safe Surfer's desktop app sets Safe Surfer up on your computer for you.  
Our aim for this project is to make it as easy as possible to get families and persons protected online on their desktops or laptops, ensuring safety and peace-of-mind.  

For enterprise/business use, we recommend to apply the DNS settings on a router which devices are connected to, please refer to the [Safe Surfer website](http://safesurfer.co.nz).  

### [Download](https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases)
[Windows (64-bit/32-bit)](https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases)  
[AppImage](https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases)  
[macOS dmg](https://gitlab.com/safesurfer/SafeSurfer-Desktop/releases)  

Windows Store (coming soon)  
macOS App Store (coming soon)  
snapcraft store (coming soon)  
Ubuntu PPA: ppa:bobymcbobs/safesurfer - safesurfer-desktop  
openSUSE and Fedora: [OBS](https://software.opensuse.org/download.html?project=home%3ABoby_MC_bobs&package=SafeSurfer-Desktop)  

## Features
- Toggle DNS settings with one button  
- Protects against harmful content  
- Easy to use and setup  
- Configure lifeguard from inside the app (requires mDNS/5353 allowed through firewall)  

## Feedback
Have you used SafeSurfer-Desktop and want to give feedback?  
Visit our [feedback site](http://safesurfer.co.nz/feedback) to leave us some feedback.  

## Building
Consult the [build manual](docs/BUILDING.md) for instructions on building and running from source.  

### Bugs
Wanna help us find and squash bugs?  
Check out our [bug reporting guide](docs/BUGS.md).  

## Contributing
Read our [contribution guide](docs/CONTRIBUTING.md) to get started!  
We'd love your help improving this project, together helping families and individuals stay safe on the internet!  

## Roadmap
If you'd like to see our plans for future versions of this app. Check out our [roadmap](docs/ROADMAP.md)  

### Translating Safe Surfer desktop
<a href="https://hosted.weblate.org/projects/safe-surfer/translations">
    <img src="https://hosted.weblate.org/widgets/safe-surfer/-/translations/multi-auto.svg" alt="Translation status" />
</a>

Note: Although some translations display as 100%, they may need review.  
Help us speak your language!  
Read our [translation guide](docs/TRANSLATING.md) to get started!  

## Notes
Known issues  
- If you are using Hyper-V, you may need to manually remove the DNS settings from each interface in Control Panel > Network and Internet > Network Connections

## System requirements
Known compatible operating systems:  
- Fedora Workstation (28, 29)  
- macOS (El Capitan, Sierra, Mojave)  
- openSUSE Leap 15  
- Ubuntu (14.04, 18.04, 18.10)  
- Windows 10 (1803, 1809)  
- Windows 7 (SP1)  
- Windows 8.1  

Minimum computer specifications:  
CPU: 1 core - 1 GHz - 64-bit or 32-bit (32-bit is Windows only)  
RAM: 512 MB of RAM  

## Repos
| Type | Site |
| - | - |
| Main | [GitLab](https://gitlab.com/safesurfer/SafeSurfer-Desktop) |
| Mirror | [GitHub](https://github.com/Safe-Surfer/SafeSurfer-Desktop) |

## License
Copyright 2018-2019 Safe Surfer.  
This project is licensed under the [GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.html) and is [Free Software](https://www.gnu.org/philosophy/free-sw.en.html).  
This program comes with absolutely no warranty.  
