# Reporting bugs
As a prerequisite, you'll need a GitLab account (if you don't have one already) -- If you don't, [click here](https://gitlab.com/users/sign_in#register-pane).  

Head over to the [GitLab issues](https://gitlab.com/safesurfer/SafeSurfer-Desktop/issues/new) page, choose the `bugs.md` template (if there is no pre-written bug template given).  

Steps to report bugs:  
1. Find a bug (you will to be able to replicated it)
2. Click on build/version information in Menu --> Info --> Version: xxxxx - Build: xxxxx, press yes to copy information and head over to issues page
3. Choose the bug template
4. Paste version information below the `App Information` header
5. Fill in the rest of the fields in the template
